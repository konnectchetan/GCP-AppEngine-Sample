## Instructions
* Just Open the CLOUD SHELL in your GCP cloud console and follow the instructions as stated below
* Set the context of shell for your project

    ```gcloud config set project your_project_id```
* Get the code
 
    ```
    git clone https://gitlab.com/konnectchetan/GCP-AppEngine-Sample/
    cd GCP-AppEngine-Sample
    ```
* Deploy the code on APP ENGINE
    
    ```gcloud app deploy```
* Once you are done with this command, you will get a URL which you will make use of to access the application
